ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"

Minitest::Reporters.use!

class ActiveSupport::TestCase
    fixtures :all
    include ApplicationHelper
  
  def is_logged_in?             # Returns true if a test user is logged in.
    !session[:user_id].nil?
  end
  
  def log_in_as(user)           # Logged in as a particular user.
    session[:user_id] = user.id
  end
end


class ActionDispatch::IntegrationTest
    
  def log_in_as(user, password: "password", remember_me: '1')         # Logged in as a particular user.
    post login_path, params: { session: { email: user.email, password: password, remember_me: remember_me } }
  end
end
