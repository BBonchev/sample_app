# Ruby on Rails sample application

## Getting started 

To get started with the app, I cloned the repo and then installed the gems:

````
$ bundle install --without production
````  

Next, I migrated the database:

````
$ rails db:migrate
````

Finally, I ran the test suite to verify that everthing is working correctly:

````
$ rails test
````

After the test passes, then I am ready to run the app in a local server: 

````
$ rails server 
````


For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).